﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;
using ExamWebApp.Providers;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ExamWebApp
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultExamApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { controller = "Exam", action = "GetExams", id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "DefaultUserApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { controller = "User", action = "GetUsers", id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "DefaultHomeApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Login", id = RouteParameter.Optional }
            );
            config.Formatters.Add(new BrowserJsonFormatter());
            config.MessageHandlers.Add(new AuthenticationHandler());
        }

        public class BrowserJsonFormatter : JsonMediaTypeFormatter
        {
            public BrowserJsonFormatter()
            {
                this.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));
                this.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
                this.SerializerSettings.Formatting = Formatting.Indented;
            }

            public override void SetDefaultContentHeaders(Type type, HttpContentHeaders headers, MediaTypeHeaderValue mediaType)
            {
                base.SetDefaultContentHeaders(type, headers, mediaType);
                headers.ContentType = new MediaTypeHeaderValue("application/json");
            }
            
        }
    }
}
