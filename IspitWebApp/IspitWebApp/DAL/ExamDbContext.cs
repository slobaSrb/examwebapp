﻿using ExamWebApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace ExamWebApp.DAL
{
    public class ExamDbContext:DbContext
    {
        public ExamDbContext():base("ExamDbContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Exam> Exams { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<CheckIn> CheckIns { get; set; }
        public DbSet<Chat> Chats { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Token> Tokens { get; set; }
        public DbSet<Login> Logins { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<User>()
                .HasMany<Exam>(s => s.Exams)
                .WithMany(c => c.Users)
                .Map(cs =>
                {
                    cs.MapLeftKey("UserRefId");
                    cs.MapRightKey("ExamRefId");
                    cs.ToTable("UserExam");
                });

            modelBuilder.Entity<Department>()
                .HasMany<Exam>(d => d.Exams)
                .WithMany(e => e.Departments)
                .Map(de =>
                {
                    de.MapLeftKey("DepartmentRefId");
                    de.MapRightKey("ExamRefId");
                    de.ToTable("DepartmentExam");
                });

            modelBuilder.Entity<Role>()
                .HasMany<User>(r => r.Users)
                .WithMany(u => u.Roles)
                .Map(ur =>
                {
                    ur.MapLeftKey("RoleRefId");
                    ur.MapRightKey("UserRefId");
                    ur.ToTable("RoleUser");
                });

            modelBuilder.Entity<Department>()
                .HasMany<User>(d => d.Users)
                .WithMany(e => e.Departments)
                .Map(de =>
                {
                    de.MapLeftKey("DepartmentRefId");
                    de.MapRightKey("UserRefId");
                    de.ToTable("DepartmentUser");
                });
        }

    }
}