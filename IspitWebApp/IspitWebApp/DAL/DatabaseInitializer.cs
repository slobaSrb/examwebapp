﻿using ExamWebApp.DAL;
using ExamWebApp.Models;
using ProjectMVC5.Controllers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ExamWebApp.DAL
{
    public class DatabaseInitializer : CreateDatabaseIfNotExists<ExamDbContext>
    {
        protected override void Seed(ExamDbContext context)
        {
            //napraviti role
            var Departments = new List<Department> {
                new Department{DepartmentName="Razvoj Softvera"},
                new Department{DepartmentName="Upravljanje Informacijama"},

            };
            Departments.ForEach(d => context.Departments.Add(d));
            context.SaveChanges();

            var Roles = new List<Role>
            {
                new Role{RoleName="Student"},
                new Role{RoleName="Professor"},
                new Role{RoleName="Administrator"}
            };
            Roles.ForEach(r => context.Roles.Add(r));
            context.SaveChanges();


            var ProtocolNo1 = HomeController.CreateSalt(4);
            var ProtocolNo2 = HomeController.CreateSalt(4);
            var ProtocolNo3 = HomeController.CreateSalt(4);
            var ProtocolNo4 = HomeController.CreateSalt(4);
            var ProtocolNo5 = HomeController.CreateSalt(4);
            var ProtocolNo6 = HomeController.CreateSalt(4);
            var ProtocolNo7 = HomeController.CreateSalt(4);
            var ProtocolNo8 = HomeController.CreateSalt(4);
            var ProtocolNo9 = HomeController.CreateSalt(4);

            var ProtocolNo10 = HomeController.CreateSalt(4);
            var ProtocolNo11 = HomeController.CreateSalt(4);
            var ProtocolNo12 = HomeController.CreateSalt(4);
            var ProtocolNo13 = HomeController.CreateSalt(4);
            var ProtocolNo14 = HomeController.CreateSalt(4);
            var ProtocolNo15 = HomeController.CreateSalt(4);
            var ProtocolNo16 = HomeController.CreateSalt(4);
            var ProtocolNo17 = HomeController.CreateSalt(4);
            var ProtocolNo18 = HomeController.CreateSalt(4);
            var ProtocolNo19 = HomeController.CreateSalt(4);
            var Users = new List<User>
            {
                new User{Approved = true, IndexNo = "Prof", Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Professor"), context.Roles.SingleOrDefault(r => r.RoleName == "Administrator")}, FirstName="Marko", LastName="Petkovic",Email="marko.petkovic@pmf.edu.rs",Password= HomeController.GetSha256("slobodan123"+"marko.petkovic@pmf.edu.rs"+ ProtocolNo1), ProtocolNo = ProtocolNo1 },
                new User{Approved = true, IndexNo = "Prof", Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Professor"), context.Roles.SingleOrDefault(r => r.RoleName == "Administrator")}, FirstName="Marko", LastName="Milosevic",Email="marko.milosevic@pmf.edu.rs",Password= HomeController.GetSha256("slobodan123"+"marko.milosevic@pmf.edu.rs".ToLower()+ProtocolNo2), ProtocolNo = ProtocolNo2},
                new User{Approved = true, IndexNo = "Prof", Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Professor"), context.Roles.SingleOrDefault(r => r.RoleName == "Administrator")}, FirstName="Svetozar", LastName="Rancic",Email="svetozar.rancic@pmf.edu.rs",Password= HomeController.GetSha256("slobodan123"+"svetozar.rancic@pmf.edu.rs".ToLower()+ProtocolNo3), ProtocolNo = ProtocolNo3},
                new User{Approved = true, IndexNo = "Prof", Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Professor") }, FirstName="Jelena", LastName="Ignjatovic",Email="jelena.ignjatovic@pmf.edu.rs",Password= HomeController.GetSha256("slobodan123"+"jelena.ignjatovic@pmf.edu.rs"+ProtocolNo4), ProtocolNo = ProtocolNo4},
                new User{Approved = true, IndexNo = "Prof", Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Professor") }, FirstName="Vesna", LastName="Velickovic",Email="Vesna.Velickovic@pmf.edu.rs",Password= HomeController.GetSha256("slobodan123"+"Vesna.Velickovic@pmf.edu.rs".ToLower()+ProtocolNo5), ProtocolNo = ProtocolNo5},
                new User{Approved = true, IndexNo = "Prof", Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Professor") }, FirstName="Aleksandar", LastName="Stamenkovic",Email="Aleksandar.Stamenkovic@pmf.edu.rs",Password= HomeController.GetSha256("slobodan123"+"Aleksandar.Stamenkovic@pmf.edu.rs".ToLower()+ProtocolNo6), ProtocolNo = ProtocolNo6},
                new User{Approved = true, IndexNo = "Prof", Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Professor") }, FirstName="Predrag", LastName="Stanimirovic",Email="Predrag.Stanimirovic@pmf.edu.rs",Password= HomeController.GetSha256("slobodan123"+"Predrag.Stanimirovic@pmf.edu.rs".ToLower()+ProtocolNo7), ProtocolNo = ProtocolNo7},
                new User{Approved = true, IndexNo = "Prof", Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Professor") }, FirstName="Milan", LastName="Tasic",Email="Milan.Tasic@pmf.edu.rs",Password= HomeController.GetSha256("slobodan123"+"Milan.Tasic@pmf.edu.rs".ToLower()+ProtocolNo8), ProtocolNo = ProtocolNo8},
                new User{Approved = true, IndexNo = "Prof", Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Professor") }, FirstName="Velimir", LastName="Ilic",Email="Velimir.Ilic@pmf.edu.rs",Password= HomeController.GetSha256("slobodan123"+"Velimir.Ilic@pmf.edu.rs".ToLower()+ProtocolNo9), ProtocolNo = ProtocolNo9},

                new User{Approved = true, Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera") }, Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Student"), context.Roles.SingleOrDefault(r => r.RoleName == "Administrator")}, FirstName="Slobodan", LastName="Stankovic",Email="slobodan.stankovic@pmf.edu.rs",IndexNo="71RN",Password= HomeController.GetSha256("slobodan123"+"slobodan.stankovic@pmf.edu.rs"+ProtocolNo10), ProtocolNo = ProtocolNo10},
                new User{Approved = true, Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera") }, Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Student")}, FirstName="Djordje", LastName="Marinkovic",Email="djordje.marinkovic@pmf.edu.rs",IndexNo="63RN",Password= HomeController.GetSha256("slobodan123"+"djordje.marinkovic@pmf.edu.rs"+ProtocolNo11), ProtocolNo = ProtocolNo11},
                new User{Approved = true, Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera") }, Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Student")}, FirstName="Nikola", LastName="Djordjevic",Email="nikola.djordjevic@pmf.edu.rs",IndexNo="73RN",Password= HomeController.GetSha256("slobodan123"+"nikola.djordjevic@pmf.edu.rs"+ProtocolNo12), ProtocolNo = ProtocolNo12 },
                new User{Approved = true, Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Upravljanje Informacijama")}, Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Student")}, FirstName="Marija", LastName="Stojanovic",Email="Marija.Stojanovic@pmf.edu.rs",IndexNo="73RN",Password= HomeController.GetSha256("slobodan123"+"Marija.Stojanovic@pmf.edu.rs".ToLower()+ProtocolNo13), ProtocolNo = ProtocolNo13 },
                new User{Approved = true, Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera") }, Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Student")}, FirstName="Veljko", LastName="Jaksic",Email="Veljko.Jaksic@pmf.edu.rs",IndexNo="73RN",Password= HomeController.GetSha256("slobodan123"+"Veljko.Jaksic@pmf.edu.rs".ToLower()+ProtocolNo14), ProtocolNo = ProtocolNo14 },
                new User{Approved = true, Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera") }, Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Student")}, FirstName="Sara", LastName="Stankovic",Email="Sara.Stankovic@pmf.edu.rs",IndexNo="73RN",Password= HomeController.GetSha256("slobodan123"+"Sara.Stankovic@pmf.edu.rs".ToLower()+ProtocolNo15), ProtocolNo = ProtocolNo15 },
                new User{Approved = true, Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Upravljanje Informacijama")}, Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Student")}, FirstName="Nenad", LastName="Todorovic",Email="Nenad.Todorovic@pmf.edu.rs",IndexNo="73RN",Password= HomeController.GetSha256("slobodan123"+"Nenad.Todorovic@pmf.edu.rs".ToLower()+ProtocolNo16), ProtocolNo = ProtocolNo16 },
                new User{Approved = true, Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera") }, Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Student")}, FirstName="Stefan", LastName="Jovanovic",Email="Stefan.Jovanovic@pmf.edu.rs",IndexNo="73RN",Password= HomeController.GetSha256("slobodan123"+"Stefan.Jovanovic@pmf.edu.rs".ToLower()+ProtocolNo17), ProtocolNo = ProtocolNo17 },
                new User{Approved = true, Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera") }, Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Student")}, FirstName="Sanja", LastName="Petrovic",Email="Sanja.Petrovic@pmf.edu.rs",IndexNo="73RN",Password= HomeController.GetSha256("slobodan123"+"Sanja.Petrovic@pmf.edu.rs".ToLower()+ProtocolNo18), ProtocolNo = ProtocolNo18 },
                new User{Approved = true, Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera") }, Roles = new List<Role>(){ context.Roles.SingleOrDefault(r => r.RoleName == "Student")}, FirstName="Radoica", LastName="Nikolic",Email="Radoica.Nikolic@pmf.edu.rs",IndexNo="73RN",Password= HomeController.GetSha256("slobodan123"+"Radoica.Nikolic@pmf.edu.rs".ToLower()+ProtocolNo19), ProtocolNo = ProtocolNo19 }


            };
            Users.ForEach(u => context.Users.Add(u));
            context.SaveChanges();
            var Exams = new List<Exam>
            {
                // ubaciti dva profesora u predmet
                new Exam{Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera"), context.Departments.SingleOrDefault(d => d.DepartmentName == "Upravljanje Informacijama")}, ExamID = 1 , ExamName="Teorija Programskih Jezika",ExamImagePath="/images/professor_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1 },
                new Exam{Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera")}, ExamName="Razvoj Veb Aplikacija",ExamImagePath="/images/arrival_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1},
                new Exam{Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera")}, ExamName="Razvoj Mobilnih Aplikacija",ExamImagePath="/images/chat_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=2 },
                new Exam{Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Upravljanje Informacijama")}, ExamName="Teorija Informacija i Kodiranje",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
                new Exam{Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera")}, ExamName="Testiranje i Metrika Softvera",ExamImagePath="/images/professor_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1},
                new Exam{Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera")}, ExamName="Dizajn Softvera",ExamImagePath="/images/arrival_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
                new Exam{Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera"), context.Departments.SingleOrDefault(d => d.DepartmentName == "Upravljanje Informacijama") }, ExamName="Teorija Algoritama Automata i Jezika",ExamImagePath="/images/chat_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1 },

                new Exam{Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Upravljanje Informacijama")}, ExamName="Kriptografski Algoritmi",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
                new Exam{Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera"), context.Departments.SingleOrDefault(d => d.DepartmentName == "Upravljanje Informacijama") }, ExamName="Metgodika Programiranja",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
                new Exam{Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera"), context.Departments.SingleOrDefault(d => d.DepartmentName == "Upravljanje Informacijama") }, ExamName="Matematicka Logika",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},

                new Exam{Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera"), context.Departments.SingleOrDefault(d => d.DepartmentName == "Upravljanje Informacijama") }, ExamName="Metodika Elektronskog Ucena",ExamImagePath="/images/student_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1},

                new Exam{Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera"), context.Departments.SingleOrDefault(d => d.DepartmentName == "Upravljanje Informacijama") }, ExamName="Racunarska Grafika 1",ExamImagePath="/images/student_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1},

                new Exam{Departments = new List<Department>(){ context.Departments.SingleOrDefault(d => d.DepartmentName == "Razvoj Softvera"), context.Departments.SingleOrDefault(d => d.DepartmentName == "Upravljanje Informacijama") }, ExamName="Baze Podataka",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},

            };
            Exams.ForEach(e => context.Exams.Add(e));
            context.SaveChanges();
        }
    }
}