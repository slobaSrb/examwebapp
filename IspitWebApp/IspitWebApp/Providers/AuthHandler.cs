﻿
using AutoMapper;
using ExamWebApp.DAL;
using ExamWebApp.Models;
using ProjectMVC5.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using IspitWebApp.Controllers;
using System.Net.Http.Headers;

namespace ExamWebApp.Providers
{
    public class AuthHandler
    {
    }

    public class CredentialChecker
    {
        public static string password { get; set; }
        private ExamDbContext db = new ExamDbContext();
        public Token CheckToken(string headerToken)
        {
            password = db.Users.ToList<User>().Where(u => u.Email.ToLower() == Base64Decode(headerToken).ToLower().Split(':')[0]).SingleOrDefault().Password;
            try
            {
                //var tokens = db.Tokens.ToList<Token>().Where(t => t.TokenValue == HomeController.GetSha256(headerToken.Split(':')[1] + headerToken.Split(':')[0] + t.EndTime.ToShortDateString())).SingleOrDefault();// && (DateTime.Now.Hour - t.EndTime.Hour < 0) && ((DateTime.Now.Minute - t.EndTime.Minute) < 0) && ((DateTime.Now.Second - t.EndTime.Second) < 0)).FirstOrDefault();
                var tokens = (from t in db.Tokens
                              select t).ToList().Where(t => t.EqualsToken(headerToken));
                //);
                return tokens.ElementAt(0);//return db.Users.Where(un => un.Email.ToLower() == username.ToLower() && un.Password == GetSha256(Base64Decode(password) + un.Email.ToLower() + un.ProtocolNo)).FirstOrDefault();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        internal User CheckRoles(string loggedUserID)
        {
            try
            {
                var Users = (from u in db.Users
                             select u).ToList().Where(u => u.UserID == int.Parse(loggedUserID) && u.Password.Equals(password)).ToList();

                // Load the Roles related to a given User.
                db.Entry(Users.ElementAt(0)).Collection(p => p.Roles).Load();
                

                return Users.ElementAt(0);
            }
            catch (Exception e)
            {
                return null;
            }
            //var User = db.Users.Include("Roles").SingleOrDefault(u => u.UserID == int.Parse(loggedUserID) && u.Password.Equals(password));
            //if (Users.SingleOrDefault() != null)
            //    for (int i = 0; i < Users.SingleOrDefault().Roles.Count; i++)
            //    {
            //        Users.SingleOrDefault().Roles.ElementAt(i).Users.Clear();
            //    }

        }
        private static string GetSha256(string str)
        {

            // Create a new instance of the hash crypto service provider.
            HashAlgorithm hashAlg = new SHA256CryptoServiceProvider();
            // Convert the data to hash to an array of Bytes.
            byte[] bytValue = System.Text.Encoding.UTF8.GetBytes(str);
            // Compute the Hash. This returns an array of Bytes.
            byte[] bytHash = hashAlg.ComputeHash(bytValue);
            // Optionally, represent the hash value as a base64-encoded string, 
            // For example, if you need to display the value or transmit it over a network.
            string base64 = Convert.ToBase64String(bytHash);

            return base64;
        }
        private static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }

    public class AuthenticationHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            //try
            //{
            var tokens = request.Headers.GetValues("Authorization").FirstOrDefault();
            if (!tokens.Contains("Login"))
            {
                var loggedUserID = request.Headers.GetValues("Authentication").FirstOrDefault();
                if (tokens != null)
                {
                    string tokensValues = tokens.Split(',')[0];
                    var cch = new CredentialChecker();
                    Token ObjToken = cch.CheckToken(tokensValues);

                    User ObjUser = cch.CheckRoles(loggedUserID) as User;
                    if (ObjToken != null && ObjUser != null)
                    {
                        List<string> roles = new List<string>();
                        if (ObjUser.Roles != null)
                            foreach (var item in ObjUser.Roles)
                            {
                                roles.Add(item.RoleName);
                            }

                        IPrincipal principal = new GenericPrincipal(new GenericIdentity(ObjUser.UserID.ToString()), roles.ToArray<string>());
                        Thread.CurrentPrincipal = principal;
                        HttpContext.Current.User = principal;
                    }
                    else
                    {
                        //The user is unauthorize and return 401 status  
                        var response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                        var tsc = new TaskCompletionSource<HttpResponseMessage>();
                        tsc.SetResult(response);
                        return tsc.Task;
                    }
                }
                else
                {
                    //Bad Request request because Authentication header is set but value is null  
                    var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                    var tsc = new TaskCompletionSource<HttpResponseMessage>();
                    tsc.SetResult(response);
                    return tsc.Task;
                }
                request.Headers.Clear();
                //request.Headers.Authorization = AuthenticationHeaderValue.Parse("");
                return base.SendAsync(request, cancellationToken);
            }
            else
            {
                request.Headers.Clear();
                //request.Headers.Authorization = AuthenticationHeaderValue.Parse("");
                IPrincipal principal = null;
                Thread.CurrentPrincipal = principal;
                HttpContext.Current.User = principal;
                return base.SendAsync(request, cancellationToken);
            }
        }
        //    catch
        //    {
        //        //User did not set Authentication header  
        //        var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
        //var tsc = new TaskCompletionSource<HttpResponseMessage>();
        //tsc.SetResult(response);
        //        return tsc.Task;
        //    }

        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        private static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }


}
