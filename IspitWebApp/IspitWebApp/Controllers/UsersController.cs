﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ExamWebApp.DAL;
using ExamWebApp.Models;
using ExamWebApp.Providers;
using ProjectMVC5.Controllers;

namespace IspitWebApp.Controllers
{
    public class UsersController : ApiController
    {
        private ExamDbContext db = new ExamDbContext();




        // GET: api/Users
        [HttpGet]
        [Authorize(Roles = "Professor, Administrator")]
        public IHttpActionResult GetUsers()
        {
            //var Users = db.Users.Include("Roles").Include("Departments").Include("Exams").Where(u => u.Approved == true);
            try
            {
                var Users = (from u in db.Users
                             select new
                             {
                                 UserID = u.UserID,
                                 IndexNo = u.IndexNo,
                                 FirstName = u.FirstName,
                                 LastName = u.LastName,
                                 Email = u.Email,
                                 Approved = u.Approved,
                                 Password = u.Email,
                                 ProtocolNo = u.Email
                             }).ToList();

                return Ok(Users);
                //foreach (var user in Users)
                //{
                //    user.Password = "BlaBlaTruc";
                //    user.ProtocolNo = "12345";
                //    if (user.Roles != null)
                //        foreach (var item in user.Roles)
                //        {
                //            if (item.Users != null) item.Users.Clear();
                //        }
                //if (user.Chats != null)
                //    foreach (var item in user.Chats)
                //    {
                //        if (item.User != null) item.User = null;
                //    }
                //if (user.Departments != null)
                //    foreach (var item in user.Departments)
                //    {
                //        if (item.Users != null) item.Users.Clear();
                //    }
                //if (user.Exams != null)
                //    foreach (var item in user.Exams)
                //    {
                //        if (item.Users != null) item.Users.Clear();
                //    }
                //if (user.CheckIns != null)
                //    foreach (var item in user.CheckIns)
                //    {
                //        if (item.User != null) item.User = null;
                //    }
                //}
            }
            catch (Exception e1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Getting all users failed"));
            }
        }
        [HttpGet]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "Professor, Administrator")]
        public IHttpActionResult GetUsersWithRole([FromUri]string role)
        {

            try
            {

                var users = (from r in db.Roles
                             where r.RoleName == role
                             select (from u in r.Users
                                     select
                                     new
                                     {
                                         Approved = u.Approved,
                                         //Chats = u.Chats,
                                         //CheckIns = u.CheckIns,
                                         //Departments = u.Departments,
                                         Email = u.Email,
                                         //Exams = u.Exams,
                                         FirstName = u.FirstName,
                                         IndexNo = u.IndexNo,
                                         LastName = u.LastName,
                                         Password = u.Email,
                                         ProtocolNo = u.ProtocolNo,
                                         Roles = u.Roles,
                                         UserID = u.UserID
                                     })).ToList();


                if (users.SingleOrDefault() == null || users.SingleOrDefault().ToList().Count == 0)
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.NotFound, "There are no users with that role"));
                }

                return Ok(users.SingleOrDefault().Where(u => u.Approved == true).ToList());
            }
            catch (Exception e1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e1.Message));

            }


        }

        [HttpPut]
        [Authorize(Roles = "Professor, Administrator")]
        public IHttpActionResult ApproveUser([FromBody]User user)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "User model not valid"));
                }


                var users = (from u in db.Users
                             where u.UserID == user.UserID
                             select u).ToList();


                if (users != null && users.Count == 1)
                {
                    users.ElementAt(0).Approved = true;

                    db.Entry(users.ElementAt(0)).State = EntityState.Modified;
                    db.SaveChanges();
                    return Ok((from u in db.Users
                               select new
                               {
                                   Approved = u.Approved,
                                   //Chats = u.Chats,
                                   //CheckIns = u.CheckIns,
                                   Departments = u.Departments,
                                   Email = u.Email,
                                   //Exams = u.Exams,
                                   FirstName = u.FirstName,
                                   IndexNo = u.IndexNo,
                                   LastName = u.LastName,
                                   Password = u.Email,
                                   ProtocolNo = u.Email,
                                   Roles = u.Roles,
                                   UserID = u.UserID
                               }
                                ).ToList());
                }
                else
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "User not found or multiple users found"));
                }



            }
            catch (Exception e1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e1.Message));
            }
        }

        [HttpPut]
        [Authorize(Roles = "Professor, Administrator")]
        public IHttpActionResult ForbidUser([FromBody]User user)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "User model not valid"));
                }

                var users = (from u in db.Users
                             where u.UserID == user.UserID
                             select u).ToList();



                if (users != null && users.Count == 1)
                {
                    users.ElementAt(0).Approved = false;
                    db.Entry(users.ElementAt(0)).State = EntityState.Modified;
                    db.SaveChanges();
                    return Ok((from u in db.Users
                               select new
                               {
                                   Approved = u.Approved,
                                   //Chats = u.Chats,
                                   //CheckIns = u.CheckIns,
                                   Departments = u.Departments,
                                   Email = u.Email,
                                   //Exams = u.Exams,
                                   FirstName = u.FirstName,
                                   IndexNo = u.IndexNo,
                                   LastName = u.LastName,
                                   Password = u.Email,
                                   ProtocolNo = u.Email,
                                   Roles = u.Roles,
                                   UserID = u.UserID
                               }
                               ).ToList());
                }
                else
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "User not found or multiple users found"));
                }



            }
            catch (Exception e1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e1.Message));
            }
        }


        // GET: api/Users/GetStudentsForExam/1
        [HttpGet]
        [Authorize(Roles = "Professor, Administrator")]
        public IHttpActionResult GetStudentsForExam(int id)
        {
            //List<User> rezUsers = new List<User>();/// = db.Users.SingleOrDefault(u => u.UserID==0);
            try
            {
                var exam = (from e in db.Exams
                            where e.ExamID == id
                            select new
                            {
                                Chats = e.Chats,
                                //CheckIns = e.CheckIns,
                                Users = (from u in e.Users
                                         select new
                                         {
                                             Approved = u.Approved,
                                             //Chats = u.Chats,
                                             //CheckIns = u.CheckIns,
                                             Departments = u.Departments,
                                             Email = u.Email,
                                             //Exams = u.Exams,
                                             FirstName = u.FirstName,
                                             IndexNo = u.IndexNo,
                                             LastName = u.LastName,
                                             Password = u.Email,
                                             ProtocolNo = u.Email,
                                             Roles = u.Roles,
                                             UserID = u.UserID
                                         }).ToList(),
                                Departments = e.Departments,
                                LevelOfStudies = e.LevelOfStudies,
                                ExamID = e.ExamID,
                                ExamImagePath = e.ExamImagePath,
                                ExamName = e.ExamName,
                                Semester = e.Semester,
                                YearOfStudies = e.YearOfStudies
                            }).ToList();


                //var role = (from r in db.Roles where r.RoleName == "Student" select r).SingleOrDefault();
                var users = (from u in exam.ElementAt(0).Users
                             where u.Approved
                             select new
                             {
                                 Approved = u.Approved,
                                 //Chats = u.Chats,
                                 //CheckIns = u.CheckIns,
                                 Departments = u.Departments,
                                 Email = u.Email,
                                 //Exams = u.Exams,
                                 FirstName = u.FirstName,
                                 IndexNo = u.IndexNo,
                                 LastName = u.LastName,
                                 Password = u.Email,
                                 ProtocolNo = u.Email,
                                 Roles = u.Roles,
                                 UserID = u.UserID
                             }).ToList();


                return Ok(users.Where(u => u.Roles.ContainsRoleWithName("Student")));
            }
            catch (Exception e1)
            {
                return BadRequest(e1.Message);
            }
        }
        // GET: api/Users/5
        [ResponseType(typeof(User))]
        [Authorize(Roles = "Professor, Administrator")]
        public IHttpActionResult GetUser(int id)
        {
            var user = (from u in db.Users
                         where u.UserID == id
                         select new
                         {
                             //Roles = (from r in u.Roles select r).ToList(),
                             Approved = u.Approved,
                             Email = u.Email,
                             FirstName = u.FirstName,
                             IndexNo = u.IndexNo,
                             LastName = u.LastName,
                             UserID = u.UserID,
                             Password = u.Email,
                             ProtocolNo = u.Email,
                             //Chats = (from c in u.Chats select c).ToList(),
                             CheckIns = (from ch in u.CheckIns select ch).ToList(),
                             //Departments = (from d in u.Departments select d).ToList(),
                             //Exams = (from e in u.Exams select e).ToList()
                         }).SingleOrDefault();
            if (user == null)
            {
                return NotFound();
            }

            foreach( var ch in user.CheckIns)
            {
                ch.Exam = null;
                ch.User = null;
            }

            return Ok(user);
        }
        //[AllowAnonymous]
        //public IHttpActionResult GetUserIDByMail([FromUri]string email)
        //{
        //    try
        //    {
        //        User user = db.Users.SingleOrDefault(u => u.Email == email);
        //        return Ok(user.UserID);
        //    }
        //    catch (Exception e1)
        //    {
        //        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.NotFound, e1.Message));
        //    }
        //}

        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        [Authorize(Roles = "Student, Professor, Administrator")]
        public IHttpActionResult PutUser(int id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.UserID)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        //// POST: api/Users
        //[ResponseType(typeof(User))]
        //public IHttpActionResult PostUser(User user)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Users.Add(user);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = user.UserID }, user);
        //}

        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        [Authorize(Roles = "Administrator")]

        public IHttpActionResult DeleteUser(int id)
        {
            User user = db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            db.SaveChanges();

            return Ok(user);
        }
        [HttpPost]
        public IHttpActionResult PostToken([FromBody] Token token)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Token model not valid"));
                }

                var userTokens = db.Tokens.Where(t => t.UserID == token.UserID);

                if (userTokens.Count() > 0)
                {
                    DeleteUserTokens(token.UserID);
                }

                token.TokenValue = HomeController.GetSha256(token.TokenValue + token.EndTime.ToShortDateString());

                db.Tokens.Add(token);


                db.SaveChanges();
                return Ok(token.EndTime);
            }
            catch (Exception e1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e1.Message));
            }
        }

        [HttpPut]
        [Authorize(Roles = "Student, Professor, Administrator")]
        public IHttpActionResult Logout([FromBody] User user)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "User model not valid"));
                }

                var userTokens = db.Tokens.Where(t => t.UserID == user.UserID);

                if (userTokens.Count() > 0)
                {
                    DeleteUserTokens(user.UserID);
                }
                return Ok(user);
            }
            catch (Exception e1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e1.Message));
            }
            return Ok();
        }


        private IHttpActionResult DeleteUserTokens(int id)
        {
            try
            {
                //get all tokens
                var allTokens = db.Tokens.Where(t => t.UserID != int.MinValue);
                //filter only user's tokens
                var userTokens = allTokens.Where(t => t.UserID == id);
                DateTime parseData;
                foreach (var item in userTokens)
                {
                    if (DateTime.TryParseExact(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"), "yyyy-MM-ddTHH:mm:ss", null, DateTimeStyles.None, out parseData))
                    {
                        db.Logins.Add(new Login() { UserID = id, LogInDateTime = item.StartTime, LogOutDateTime = parseData });
                    }
                    db.Tokens.Remove(item);
                }


                db.SaveChanges();
            }
            catch (Exception e1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e1.Message));
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return db.Users.Count(e => e.UserID == id) > 0;
        }
    }
}