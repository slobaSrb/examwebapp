﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;
using ExamWebApp.DAL;
using ExamWebApp.Models;
using Newtonsoft.Json;

namespace ProjectMVC5.Controllers
{
    [Authorize]
    public class HomeController : ApiController
    {
        private ExamDbContext db = new ExamDbContext();



        // GET: Home
        /*        public ActionResult Index()
                {
                    if (Session["idUser"] != null)
                    {
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("Login");
                    }
                }*/

        //GET: Register

        //public ActionResult Register()
        //{
        //    return View();
        //}

        //POST: Register
        [System.Web.Http.HttpPost]
        [ResponseType(typeof(User))]
        [AllowAnonymous]
        public IHttpActionResult Register([FromBody] User user)
        {
            if (ModelState.IsValid)
            {
                var check = db.Users.FirstOrDefault(s => s.Email == user.Email);
                if (check == null)
                {
                    var studentRoles = db.Roles.Where(r => r.RoleName == "Student").ToList();
                    var salt = CreateSalt(4);
                    user.Roles = studentRoles;
                    user.Email = user.Email.ToLower();
                    user.ProtocolNo = salt;
                    user.Password = GetSha256(Base64Decode(user.Password) + user.Email.ToLower() + salt);
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.Users.Add(user);
                    db.SaveChanges();
                    user.Roles = null;
                    return Ok(user);
                }
                else
                {
                    //ViewBag.error = "Email already exists";
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Email already exists"));
                }


            }
            return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "User model is bad"));


        }


        //public IHttpActionResult Login()
        //{
        //    return Ok();
        //}



        [System.Web.Http.HttpPut]
        [ResponseType(typeof(User))]
        [AllowAnonymous]
        public IHttpActionResult Login([FromBody]User userReq)
        {
            if (ModelState.IsValid)
            {

                //var user = db.Users.Where(s => s.Email.ToLower().Equals(userReq.Email.ToLower())).Include(i => i.Roles.Select(it => it.RoleID)).Include(i => i.Roles.Select(it => it.RoleName)).ToList();

                var userQ =
                (from user in db.Users
                 where user.Email.ToLower() == userReq.Email.ToLower()
                 select user).ToList();


                if (userQ != null && userQ.Count==1)
                {
                    var sha_password = GetSha256(Base64Decode(userReq.Password) + userReq.Email.ToLower() + userQ.ElementAt(0).ProtocolNo);
                    //var data = db.Users.Where(s => s.Email.ToLower().Equals(userReq.Email.ToLower()) && s.Password.Equals(sha_password)).Include(x => x.Roles.Select(r => new Role() { RoleID = r.RoleID, RoleName = r.RoleName, Users = null })).ToList();

                    var data = (from user in db.Users
                                where user.Password == sha_password && user.Email.ToLower() == userReq.Email.ToLower()
                                select new
                                {
                                    Roles = user.Roles,
                                    Approved = user.Approved,
                                    Email = user.Email,
                                    FirstName = user.FirstName,
                                    IndexNo = user.IndexNo,
                                    LastName = user.LastName,
                                    UserID = user.UserID,
                                    Password = user.Email,
                                    ProtocolNo = user.Email,
                                    //Chats = user.Chats,
                                    //CheckIns = user.CheckIns,
                                    //Departments = user.Departments,
                                    //Exams = user.Exams

                                }).ToList();

                    
                    if (data != null && data.Count == 1)
                    {
                        return Ok(data.ElementAt(0));
                    }
                    else
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.NotFound, "User not found, contact admin if problem persists"));
                    }
                }
                else
                {
                    //ViewBag.error = "Login failed";
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.NotFound, "User not found, contact admin if problem persists"));
                }
            }
            return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "User model probably bad"));
        }

        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        private static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        //create a string Sha256
        public static string GetSha256(string str)
        {

            // Create a new instance of the hash crypto service provider.
            HashAlgorithm hashAlg = new SHA256CryptoServiceProvider();
            // Convert the data to hash to an array of Bytes.
            byte[] bytValue = System.Text.Encoding.UTF8.GetBytes(str);
            // Compute the Hash. This returns an array of Bytes.
            byte[] bytHash = hashAlg.ComputeHash(bytValue);
            // Optionally, represent the hash value as a base64-encoded string, 
            // For example, if you need to display the value or transmit it over a network.
            string base64 = Convert.ToBase64String(bytHash);

            return base64;
        }

        public static string CreateSalt(int size)
        {
            // Generate a cryptographic random number using the cryptographic 
            // service provider
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[size];
            rng.GetBytes(buff);
            // Return a Base64 string representation of the random number
            return Convert.ToBase64String(buff);
        }

    }
}