﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using ExamWebApp.DAL;
using ExamWebApp.Models;
using ExamWebApp.Providers;
using Newtonsoft.Json;

namespace ExamWebApp.Controllers
{
    public class ExamsController : ApiController
    {
        private ExamDbContext db = new ExamDbContext();

        // GET: api/Exams
        [HttpGet]
        [Authorize(Roles = "Student, Professor, Administrator")]
        public IHttpActionResult GetExams()
        {
            try
            {
                //var Exams = db.Exams.Include("Users").Include("Departments").ToList<Exam>();

                var Exams = (from exam in db.Exams
                             select new
                             {
                                 Chats = exam.Chats,
                                 //CheckIns = exam.CheckIns,
                                 //Users = exam.Users,
                                 Departments = exam.Departments,
                                 LevelOfStudies = exam.LevelOfStudies,
                                 ExamID = exam.ExamID,
                                 ExamImagePath = exam.ExamImagePath,
                                 ExamName = exam.ExamName,
                                 Semester = exam.Semester,
                                 YearOfStudies = exam.YearOfStudies
                             }).ToList();
                if (Exams == null || Exams.Count == 0)
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.NotFound, "No Exams found"));
                }
                //for (var i = 0; i < Exams.Count(); i++)
                //{
                //    if (Exams.ElementAt(i).Departments != null)
                //        for (var j = 0; j < Exams.ElementAt(i).Departments.Count; j++)
                //        {
                //            if (Exams.ElementAt(i).Departments.ElementAt(j).Users != null) Exams.ElementAt(i).Departments.ElementAt(j).Users.Clear();
                //            if (Exams.ElementAt(i).Departments.ElementAt(j).Exams != null) Exams.ElementAt(i).Departments.ElementAt(j).Exams.Clear();
                //        }
                //    if (Exams.ElementAt(i).Users != null)
                //        for (var j = 0; j < Exams.ElementAt(i).Users.Count; j++)
                //        {
                //            Exams.ElementAt(i).Users.ElementAt(j).Password = "BlaBlaTruc";
                //            Exams.ElementAt(i).Users.ElementAt(j).ProtocolNo = "12345";
                //            if (Exams.ElementAt(i).Users.ElementAt(j).Exams != null) Exams.ElementAt(i).Users.ElementAt(j).Exams.Clear();
                //            if (Exams.ElementAt(i).Users.ElementAt(j).Departments != null) Exams.ElementAt(i).Users.ElementAt(j).Departments.Clear();
                //        }
                //}


                return Ok(Exams);
            }
            catch (Exception e1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error in AllExams method"));
            }


            //return Ok(db.Exams.ToList<Exam>());
        }
        // GET: api/Exams/AllExamsForUser/userId
        [HttpGet]
        [Authorize(Roles = "Student, Professor, Administrator")]
        public IHttpActionResult AllExamsForUser([FromUri]int id)
        {

            try
            {
                //var Exams = db.Users.Include("Exams").SingleOrDefault(u => u.UserID == id).Exams.ToList<Exam>();
                //var users = (from u in db.Users
                //             where u.UserID == id
                //             select u).ToList();

                var users = (from u in db.Users
                             where u.UserID == id
                             select new
                             {
                                 Roles = u.Roles,
                                 Approved = u.Approved,
                                 Email = u.Email,
                                 FirstName = u.FirstName,
                                 IndexNo = u.IndexNo,
                                 LastName = u.LastName,
                                 UserID = u.UserID,
                                 Password = u.Email,
                                 ProtocolNo = u.Email,
                                 //Chats = u.Chats,
                                 //CheckIns = u.CheckIns,
                                 Departments = u.Departments,
                                 Exams = u.Exams

                             }).ToList();

                if (users != null && users.Count == 1)
                {
                    if (users.ElementAt(0).Approved == false)
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.NotFound, "User is not approved"));
                    }
                }
                else if (users == null || users.Count == 0)
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.Forbidden, "No Exams for that user"));
                }

                if (users != null && users.Count == 1)
                {
                    var exams = users.SingleOrDefault().Exams;
                    //var exams = (from e in db.Exams
                    //             where Exams
                    //             select e).ToList();
                    //users.ElementAt(0).Roles.Contains((from r in db.Roles where r.RoleName == "Administrator" select r).ToList().ElementAt(0))
                    if (users.ElementAt(0).Roles.ContainsRoleWithName("Administrator"))
                    {
                        exams.AddList(db.Exams.ToList());
                    }

                    //var Exams = (from exam in db.Exams
                    //             select new
                    //             {
                    //                 Chats = exam.Chats,
                    //                 CheckIns = exam.CheckIns,
                    //                 Users = exam.Users,
                    //                 Departments = exam.Departments,
                    //                 LevelOfStudies = exam.LevelOfStudies,
                    //                 ExamID = exam.ExamID,
                    //                 ExamImagePath = exam.ExamImagePath,
                    //                 ExamName = exam.ExamName,
                    //                 Semester = exam.Semester,
                    //                 YearOfStudies = exam.YearOfStudies
                    //             }).ToList();
                    //var exams = Exams.Where(e => e.Users.Contains(users.ElementAt(0)));
                    ////     db.Exams.Select(e => new
                    //     {
                    //         Field = e.Users.Where(u => u.UserID == id)
                    //.Concat(queryable2.Where(u => !e.IsX))
                    //     });


                    //             var exams = db.Exams
                    //.Where(c => c.Users.Contains(users.First()))
                    //.Select(exam => new
                    //{
                    //    Chats = exam.Chats,
                    //    CheckIns = exam.CheckIns,
                    //    Users = exam.Users,
                    //    Departments = exam.Departments,
                    //    LevelOfStudies = exam.LevelOfStudies,
                    //    ExamID = exam.ExamID,
                    //    ExamImagePath = exam.ExamImagePath,
                    //    ExamName = exam.ExamName,
                    //    Semester = exam.Semester,
                    //    YearOfStudies = exam.YearOfStudies
                    //}).ToList();

                    //if (users.ElementAt(0).Roles.Contains((from r in db.Roles where r.RoleName == "Administrator" select r).ToList().ElementAt(0)))
                    //{
                    //exams.AddList(db.Exams.Where(e => !e.Users.Contains(users.First())).Select(exam =>

                    //new
                    //{
                    //    Chats = exam.Chats,
                    //    CheckIns = exam.CheckIns,
                    //     Users = exam.Users,
                    //     Departments = exam.Departments,
                    //    LevelOfStudies = exam.LevelOfStudies,
                    //    ExamID = exam.ExamID,
                    //    ExamImagePath = exam.ExamImagePath,
                    //    ExamName = exam.ExamName,
                    //    Semester = exam.Semester,
                    //    YearOfStudies = exam.YearOfStudies
                    //}).ToList());
                    //}
                    return Ok(exams);
                }
                else
                {
                    if (users.Count == 0)
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.NotFound, "No User with that ID"));
                    }
                    else
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "There are more users with that ID"));
                    }
                }

            }
            catch (Exception e1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e1.Message));
            }
        }


        // GET: api/Exams/5
        [ResponseType(typeof(Exam))]
        [Authorize(Roles = "Administrator")]
        public IHttpActionResult GetExam(int id)
        {
            Exam exam = db.Exams.Find(id);
            if (exam == null)
            {
                return NotFound();
            }

            return Ok(exam);
        }

        // PUT: api/Exams/5
        [ResponseType(typeof(void))]
        [HttpPut]
        [Authorize(Roles = "Professor, Administrator")]
        public IHttpActionResult PutExam(int id, Exam exam)
        {
            //Exam exam = JsonConvert.DeserializeObject(examStr) as Exam;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != exam.ExamID)
            {
                return BadRequest();
            }
            User user = exam.Users.ElementAt(0);
            var model = db.Exams.Include("Users").SingleOrDefault(e => e.ExamID == exam.ExamID);
            var subUsers = db.Users.Where(u => u.UserID == user.UserID).ToList();
            if (model.Users.Contains(subUsers.ElementAt(0)))
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.Conflict, "User already assingned/added to an Exam"));
            }

            model.Users.Add(subUsers.ElementAt(0));

            db.Entry(model).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
                var userFin = (from u in subUsers
                               select new
                               {
                                   //Roles = u.Roles,
                                   Approved = u.Approved,
                                   Email = u.Email,
                                   FirstName = u.FirstName,
                                   IndexNo = u.IndexNo,
                                   LastName = u.LastName,
                                   UserID = u.UserID,
                                   Password = u.Email,
                                   ProtocolNo = u.Email,
                                   //Chats = u.Chats,
                                   //CheckIns = u.CheckIns,
                                   //Departments = u.Departments,
                                   //Exams = u.Exams
                               }).SingleOrDefault();

                return Ok(userFin);
            }
            catch (Exception e1)
            {
                if (!ExamExists(id))
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Exam not found"));
                }
                else
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e1.Message));
                }
            }


        }


        [ResponseType(typeof(void))]
        [HttpPut]
        [Authorize(Roles = "Professor, Administrator")]
        public IHttpActionResult UnEnroll(int id, [FromBody] string buttonName)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Exam not found contact Admin"));
                }

                int userID = int.Parse(buttonName.Split('_')[1]);
                //User user = exam.Users.ElementAt(0);
                var examDb = db.Exams.Include("Users").SingleOrDefault(e => e.ExamID == id);
                //var exam = (from e in db.Exams
                //            where e.ExamID == id
                //            select new
                //            {
                //                Chats = e.Chats,
                //                CheckIns = e.CheckIns,
                //                Departments = e.Departments,
                //                Users = e.Users,
                //                ExamImagePath = e.ExamImagePath,
                //                ExamName = e.ExamName,
                //                LevelOfStudies = e.LevelOfStudies,
                //                Semester = e.Semester,
                //                YearOfStudies = e.YearOfStudies,
                //                ExamID = e.ExamID
                //            }).SingleOrDefault().Users.ToList();
                var subUser = db.Users.Include("Exams").Where(u => u.UserID == userID).SingleOrDefault();
                if (examDb != null)
                {
                    examDb.Users.Remove(subUser);
                    db.Entry(examDb).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Exam not found contact Admin"));
                }



                //for (var i = 0; i < exam.Users.Count; i++)
                //{
                //    exam.Users.ElementAt(i).Password = "BlaBlaTrue";
                //    exam.Users.ElementAt(i).ProtocolNo = "12345";

                //    if (exam.Users.ElementAt(i).Chats != null) { exam.Users.ElementAt(i).Chats.Clear(); }
                //    if (exam.Users.ElementAt(i).CheckIns != null) { exam.Users.ElementAt(i).CheckIns.Clear(); }
                //    if (exam.Users.ElementAt(i).Departments != null) { exam.Users.ElementAt(i).CheckIns.Clear(); }
                //    if (exam.Users.ElementAt(i).Exams != null) { exam.Users.ElementAt(i).Departments.Clear(); }
                //    if (exam.Users.ElementAt(i).Roles != null) { exam.Users.ElementAt(i).Roles.Clear(); }
                //}
                var finalExamWithUsers = (from e in db.Exams
                                          where e.ExamID == id
                                          select new
                                          {
                                              //Chats = e.Chats,
                                              //CheckIns = e.CheckIns,
                                              //Departments = e.Departments,
                                              Users = (from u in e.Users
                                                       select new
                                                       {
                                                           Approved = u.Approved,
                                                           //Chats = u.Chats,
                                                           //CheckIns = u.CheckIns,
                                                           Departments = u.Departments,
                                                           Email = u.Email,
                                                           //Exams = u.Exams,
                                                           FirstName = u.FirstName,
                                                           IndexNo = u.IndexNo,
                                                           LastName = u.LastName,
                                                           Password = u.Email,
                                                           ProtocolNo = u.Email,
                                                           Roles = u.Roles,
                                                           UserID = u.UserID
                                                       }).ToList(),
                                              ExamImagePath = e.ExamImagePath,
                                              ExamName = e.ExamName,
                                              LevelOfStudies = e.LevelOfStudies,
                                              Semester = e.Semester,
                                              YearOfStudies = e.YearOfStudies,
                                              ExamID = e.ExamID
                                          }).ToList();
                return Ok(finalExamWithUsers.ElementAt(0).Users.Where(u => u.Roles.ContainsRoleWithName("Student")));
            }
            catch (Exception e1/*DbUpdateConcurrencyException*/)
            {
                if (!ExamExists(id))
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Exam not found contact Admin"));
                }
                else
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.NotFound, e1.Message));
                }
            }

            return BadRequest();
        }
        [ResponseType(typeof(CheckIn))]
        [HttpPut]
        [Authorize(Roles = "Student, Professor, Administrator")]
        public IHttpActionResult PutCheckIn(int id, [FromBody]  User student)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "CheckIn Model not valid"));
                }
                var exam = db.Exams.Include("CheckIns").Include("Users").Where(e => e.ExamID == id).SingleOrDefault();

                var stud = db.Users.SingleOrDefault(s => s.UserID == student.UserID && s.Approved == true);

                if (!exam.Users.Contains(stud))
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Student is not in the exam"));
                }
                var checkIns = db.CheckIns.Where(c => c.UserID == student.UserID && c.ExamID == id).ToList();
                checkIns = checkIns.Where(ch => ch.CheckInDateTime.ToShortDateString() == DateTime.Now.ToShortDateString()).ToList();
                if (checkIns.Count != 0)
                {

                    stud.Approved = false;
                    db.Entry(stud).State = EntityState.Modified;
                    db.SaveChanges();
                    DeleteCheckIns(checkIns);
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.Forbidden, "CheckIn exists and student is disabled"));
                }

                //List<CheckIn> checkIns = exam.CheckIns.ToList<CheckIn>();
                //exam.CheckIns.Clear();
                var checkIn = new CheckIn();
                DateTime dateValue = DateTime.Now;
                DateTime parseData;
                //string pattern = "YYYY-MM-DDTHH:mm:ss";
                string pattern = "yyyy-MM-ddTHH:mm:ss";
                if (DateTime.TryParseExact(dateValue.ToString(pattern), pattern, null, DateTimeStyles.None, out parseData))
                {
                    checkIn.CheckInDateTime = parseData;
                }
                else
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "CheckIn didn't saved"));
                }
                exam.Users = null;
                checkIn.ExamID = id;
                checkIn.UserID = student.UserID;
                exam.CheckIns.Add(checkIn);
                db.Exams.Add(exam);
                db.Entry(exam).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExamExists(id))
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Exam not found"));
                    }
                    else
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "CheckIn didn't saved"));
                    }
                }
                var checkList = new List<CheckIn>();
                checkList.Add(checkIn);
                var checkIn2 = (from c in checkList
                                select new
                                {
                                    CheckInDateTime = c.CheckInDateTime,
                                    CheckInID = c.CheckInID,
                                    CheckOutDateTime = c.CheckOutDateTime,
                                    ExamID = c.ExamID,
                                    FilePath = c.FilePath,
                                    UserID = c.UserID
                                }).SingleOrDefault();
                //if (checkIn.Exam != null) checkIn.Exam.CheckIns.Clear();
                //if (checkIn.User != null)
                //{
                //    checkIn.User.CheckIns.Clear();
                //    checkIn.User.Password = "BlaBlaTruc";
                //    checkIn.User.ProtocolNo = "12345";
                //}
                return Ok(checkIn2);
            }
            catch (Exception e1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Internal Server Error"));
            }
            // return CreatedAtRoute("DefaultApi", new { id = exam.ExamID }, exam);
        }

        private IHttpActionResult DeleteCheckIns(List<CheckIn> checkIns)
        {
            try
            {
                //get all checkins
                var allCheckins = db.CheckIns.Where(t => t.CheckInID != 0).ToList();
                //filter only user's checkIns
                var userCheckIns = allCheckins.Where(t => t.UserID == checkIns.ElementAt(0).UserID).ToList();
                var orderedCheckIns = userCheckIns.OrderBy(c => c.CheckInID).ToList();
                foreach (var item in orderedCheckIns)
                {
                    if (orderedCheckIns.IndexOf(item) == (orderedCheckIns.Count - 1)) break;
                    db.CheckIns.Remove(item);
                }


                db.SaveChanges();
                return Ok();
            }
            catch (Exception e1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e1.Message));
            }

        }

        [ResponseType(typeof(CheckIn))]
        [HttpPut]
        [Authorize(Roles = "Student, Professor, Administrator")]
        public IHttpActionResult PutCheckOut(int id, [FromBody]  User student)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var checks = (from c in db.CheckIns
                              where c.UserID == student.UserID && c.ExamID == id
                              select c).ToList().OrderBy(c => c.CheckInID);

                var ch = checks.SingleOrDefault();
                ch.FilePath = "NA";
                string pattern = "yyyy-MM-ddTHH:mm:ss";
                DateTime parseData;

                if (DateTime.TryParseExact(DateTime.Now.ToString(pattern), pattern, null, DateTimeStyles.None, out parseData))
                {
                    ch.CheckOutDateTime = parseData;
                }
                else
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Dates didn\'t parse"));
                }
                db.Entry(ch).State = EntityState.Modified;
                db.SaveChanges();
                var stud = db.Users.SingleOrDefault(s => s.UserID == student.UserID);
                stud.Approved = false;
                db.Entry(stud).State = EntityState.Modified;
                db.SaveChanges();

                return Ok("Student chechked out and disabled");
            }
            catch (Exception e1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e1.Message));
            }
            // return CreatedAtRoute("DefaultApi", new { id = exam.ExamID }, exam);
        }



        [ResponseType(typeof(Exam))]
        [HttpPut]
        [Authorize(Roles = "Professor, Administrator")]
        public IHttpActionResult PutFiles([FromBody] Exam examBody)
        {


            if (!ModelState.IsValid)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "exam model not valid"));
            }
            var checkIns = db.CheckIns.Where(c => c.ExamID == examBody.ExamID).ToList();
            if (checkIns.Count != 0)
            {
                return Ok(checkIns);
            }
            else
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "files don't exist for this exam"));
            }
        }

        [ResponseType(typeof(CheckIn))]
        [HttpPut]
        [Authorize(Roles = "Student")]
        public IHttpActionResult UploadFile([FromBody] CheckIn checkIn)
        {


            if (!ModelState.IsValid)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "check in model not valid"));
            }

            var exams = (from e in db.Exams where e.ExamID == checkIn.ExamID select e).ToList();
            var user = (from u in db.Users where u.UserID == checkIn.UserID select u).ToList();
            if (exams.Count != 1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "There is no exam with that id"));
            }

            if (user.Count != 1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "There is no user with that id"));
            }

            Byte[] bytes = Convert.FromBase64String(checkIn.FilePath);

            if ((bytes != null) && (bytes.Length > 0))
            {
                try
                {


                    var folder = HttpContext.Current.Server.MapPath("~") + "\\ExamFiles\\" + exams.ElementAt(0).ExamName;
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }


                    string path = HttpContext.Current.Server.MapPath("~") + "\\ExamFiles\\" + exams.ElementAt(0).ExamName;
                    path += "\\" + user.ElementAt(0).FullName().Replace(" ", "_") + "_" + user.ElementAt(0).IndexNo + "_" + DateTime.Now.ToString("dd/MM/yy-H/mm/ss") + ".zip";

                    File.WriteAllBytes(path, bytes);

                    string pattern = "yyyy-MM-ddTHH:mm:ss";
                    DateTime dateValue = DateTime.Now;
                    DateTime parseData;
                    if (DateTime.TryParseExact(dateValue.ToString(pattern), pattern, null, DateTimeStyles.None, out parseData))
                    {
                        checkIn.CheckOutDateTime = parseData;
                    }
                    else
                    {
                        return BadRequest();
                    }
                    checkIn.FilePath = path;

                    db.Entry(checkIn).State = EntityState.Modified;

                    db.SaveChanges();

                    //File.WriteAllBytes("..\\ExamFiles\\" + exams.ElementAt(0).ExamName +"\\" + user.ElementAt(0).FullName(), bytes);
                    return Ok();
                }
                catch (Exception ex)
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message));
                }
                //string fn = System.IO.Path.GetFileName(upload.FileName);
                //string pathString2 = "..\\ExamFiles\\"+exams.ElementAt(0).ExamName;
                //string SaveLocation = pathString2 + fn;
                //try
                //{
                //    upload.SaveAs(SaveLocation);
                //    return Ok(checkIn);
                //}
                //catch (Exception ex)
                //{
                //    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message));
                //    //Note: Exception.Message returns detailed message that describes the current exception. 
                //    //For security reasons, we do not recommend you return Exception.Message to end users in 
                //    //production environments. It would be better just to put a generic error message. 
                //}
            }
            else
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Please select a file to upload."));
            }
        }

        [HttpPost]
        [Authorize(Roles = "Professor")]
        public IHttpActionResult UploadFileProf([FromBody] CheckIn checkIn)
        {


            if (!ModelState.IsValid)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "check in model not valid"));
            }

            var exams = (from e in db.Exams where e.ExamID == checkIn.ExamID select e).ToList();
            var user = (from u in db.Users where u.UserID == checkIn.UserID select u).ToList();
            if (exams.Count != 1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "There is no exam with that id"));
            }

            if (user.Count != 1)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "There is no user with that id"));
            }

            Byte[] bytes = Convert.FromBase64String(checkIn.FilePath);

            if ((bytes != null) && (bytes.Length > 0))
            {
                try
                {


                    var folder = HttpContext.Current.Server.MapPath("~") + "\\ExamFiles\\" + exams.ElementAt(0).ExamName;
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }


                    string path = HttpContext.Current.Server.MapPath("~") + "\\ExamFiles\\" + exams.ElementAt(0).ExamName;
                    path += "\\" + user.ElementAt(0).FullName().Replace(" ", "_") + "_" + user.ElementAt(0).IndexNo + "_" + DateTime.Now.ToString("dd/MM/yy-H/mm/ss") + ".zip";

                    File.WriteAllBytes(path, bytes);

                    string pattern = "yyyy-MM-ddTHH:mm:ss";
                    DateTime dateValue = DateTime.Now;
                    DateTime parseData;
                    if (DateTime.TryParseExact(dateValue.ToString(pattern), pattern, null, DateTimeStyles.None, out parseData))
                    {
                        checkIn.CheckOutDateTime = parseData;
                    }
                    else
                    {
                        return BadRequest();
                    }
                    checkIn.FilePath = path;

                    db.CheckIns.Add(checkIn);//.State = EntityState.Modified;

                    db.SaveChanges();
                    checkIn.Exam = null;
                    checkIn.User = null;
                    //File.WriteAllBytes("..\\ExamFiles\\" + exams.ElementAt(0).ExamName +"\\" + user.ElementAt(0).FullName(), bytes);
                    return Ok(checkIn);
                }
                catch (Exception ex)
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message));
                }
            }
            else
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Please select a file to upload."));
            }
        }
        // POST: api/Exams
        [ResponseType(typeof(Exam))]
        [Authorize(Roles = "Administrator")]
        public IHttpActionResult PostExam(Exam exam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Exams.Add(exam);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = exam.ExamID }, exam);
        }

        // DELETE: api/Exams/5
        [ResponseType(typeof(Exam))]
        [Authorize(Roles = "Administrator")]

        public IHttpActionResult DeleteExam(int id)
        {
            Exam exam = db.Exams.Find(id);
            if (exam == null)
            {
                return NotFound();
            }

            db.Exams.Remove(exam);
            db.SaveChanges();

            return Ok(exam);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ExamExists(int id)
        {
            return db.Exams.Count(e => e.ExamID == id) > 0;
        }
    }
}