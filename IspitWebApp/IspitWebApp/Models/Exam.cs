﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Windows.Media.Imaging;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExamWebApp.Models
{
    public class Exam
    {
        [Key]
        public int ExamID { get; set; }
        public string ExamName { get; set; }
        public int YearOfStudies { get; set; }
        public int Semester { get; set; }
        public string LevelOfStudies { get; set; }
        public string ExamImagePath { get; set; }
        [ForeignKey("ChatID")]
        public virtual ICollection<Chat> Chats { get; set; }
        //List<File> files = new List<File>();
        [ForeignKey("CheckInID")]
        public virtual ICollection<CheckIn> CheckIns { get; set; }
        [ForeignKey("UserID")]
        public virtual ICollection<User> Users { get; set; }
        //[ForeignKey("Departments")]
        //public int DepartmentID { get; set; }
        [ForeignKey("DepartmentID")]
        public virtual ICollection<Department> Departments { get; set; }

    }
}