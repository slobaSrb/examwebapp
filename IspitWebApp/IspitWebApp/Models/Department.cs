﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ExamWebApp.Models
{
    public class Department
    {
        [Key]
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        [ForeignKey("ExamID")]
        public virtual ICollection<Exam> Exams { get; set; }
        [ForeignKey("UserID")]
        public virtual ICollection<User> Users { get; set; }
    }
}