﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ExamWebApp.Models
{
    public class User
    {
        [Key, Column(Order = 1)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserID { get; set; }
        public string? IndexNo { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string LastName { get; set; }
        [Required]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}")]
        public string Email { get; set; }
        [Required]
        //[RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,15}$")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        //[NotMapped]
        //[Required]
        //[System.ComponentModel.DataAnnotations.Compare("Password")]
        //public string ConfirmPassword { get; set; }
        public string ProtocolNo { get; set; }
        public bool Approved { get; set; }
        public string FullName() { return this.FirstName + " " + this.LastName; }
        //public string Title { get; set; }
        //[ForeignKey("Chat")]
        //public int ChatID { get; set; }
        [ForeignKey("ChatID")]
        public virtual ICollection<Chat> Chats { get; set; }
        [ForeignKey("ExamID")]
        public virtual ICollection<Exam> Exams { get; set; }
        [ForeignKey("CheckInID")]
        public virtual ICollection<CheckIn> CheckIns { get; set; }
        [ForeignKey("RoleID")]
        public virtual ICollection<Role> Roles { get; set; }
        [ForeignKey("DepartmentID")]
        public virtual ICollection<Department> Departments { get; set; }
    }

}