﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExamWebApp.Models
{
    public class Login
    {
        [Key]
        public int LoginID { get; set; }
        public DateTime LogInDateTime { get; set; }
        public DateTime LogOutDateTime { get; set; }
        public int UserID { get; set; }
    }
}