namespace ExamWebApp.Migrations
{
    using ExamWebApp.DAL;
    using ExamWebApp.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ExamWebApp.DAL.ExamDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
        //protected override void Seed(ExamDbContext context)
        //{
        //    //napraviti role
        //    var Departments = new List<Department> {
        //        new Department{DepartmentName="Razvoj Softvera"},
        //        new Department{DepartmentName="Upravljanje Informacijama"},

        //    };
        //    Departments.ForEach(d => context.Departments.Add(d));
        //    var Users = new List<User>
        //    {
        //        new User{FirstName="Marko", LastName="Petkovic",Email="marko.petkovic@pmf.edu.rs",Password="slobodan123"},
        //        new User{FirstName="Marko", LastName="Milosevic",Email="marko.milosevic@pmf.edu.rs",Password="slobodan123"},
        //        new User{FirstName="Svetozar", LastName="Rancic",Email="svetozar.rancic@pmf.edu.rs",Password="slobodan123"},
        //        new User{FirstName="Jelena", LastName="Ignjatovic",Email="jelena.ignjatovic@pmf.edu.rs",Password="slobodan123"},
        //        new User{FirstName="Vesna", LastName="Velickovic",Email="Vesna.Velickovic@pmf.edu.rs",Password="slobodan123"},
        //        new User{FirstName="Aleksandar", LastName="Stamenkovic",Email="Aleksandar.Stamenkovic@pmf.edu.rs",Password="slobodan123"},
        //        new User{FirstName="Predrag", LastName="Stanimirovic",Email="Predrag.Stanimirovic@pmf.edu.rs",Password="slobodan123"},
        //        new User{FirstName="Milan", LastName="Tasic",Email="Milan.Tasic@pmf.edu.rs",Password="slobodan123"},
        //        new User{FirstName="Velimir", LastName="Ilic",Email="Velimir.Ilic@pmf.edu.rs",Password="slobodan123"},

        //        new User{FirstName="Slobodan", LastName="Stankovic",Email="slobodan.stankovic@pmf.edu.rs",IndexNo="71RN",Password="slobodan123"},
        //        new User{FirstName="Djordje", LastName="Marinkovic",Email="djordje.marinkovic@pmf.edu.rs",IndexNo="63RN",Password="slobodan123"},
        //        new User{FirstName="Nikola", LastName="Djordjevic",Email="nikola.djordjevic@pmf.edu.rs",IndexNo="73RN",Password="slobodan123"},
        //        new User{FirstName="Marija", LastName="Stojanovic",Email="Marija.Stojanovic@pmf.edu.rs",IndexNo="73RN",Password="slobodan123"},
        //        new User{FirstName="Veljko", LastName="Jaksic",Email="Veljko.Jaksic@pmf.edu.rs",IndexNo="73RN",Password="slobodan123"},
        //        new User{FirstName="Sara", LastName="Stankovic",Email="Sara.Stankovic@pmf.edu.rs",IndexNo="73RN",Password="slobodan123"},
        //        new User{FirstName="Nenad", LastName="Todorovic",Email="Nenad.Todorovic@pmf.edu.rs",IndexNo="73RN",Password="slobodan123"},
        //        new User{FirstName="Stefan", LastName="Jovanovic",Email="Stefan.Jovanovic@pmf.edu.rs",IndexNo="73RN",Password="slobodan123"},
        //        new User{FirstName="Sanja", LastName="Petrovic",Email="Sanja.Petrovic@pmf.edu.rs",IndexNo="73RN",Password="slobodan123"},
        //        new User{FirstName="Radoica", LastName="Nikolic",Email="Radoica.Nikolic@pmf.edu.rs",IndexNo="73RN",Password="slobodan123"}


        //    };
        //    Users.ForEach(u => context.Users.Add(u));
        //    var Exams = new List<Exam>
        //    {
        //        // ubaciti dva profesora u predmet
        //        new Exam{ ExamID = 1 , DepartmentID = 1, ExamName="Teorija Programskih Jezika",ExamImagePath="/images/professor_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{ExamID = 1 , DepartmentID = 2, ExamName="Teorija Programskih Jezika",ExamImagePath="/images/professor_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{DepartmentID = 1, ExamName="Razvoj Veb Aplikacija",ExamImagePath="/images/arrival_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{DepartmentID = 1, ExamName="Razvoj Mobilnih Aplikacija",ExamImagePath="/images/chat_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=2 },
        //        new Exam{DepartmentID = 2, ExamName="Teorija Informacija i Kodiranje",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{DepartmentID = 1, ExamName="Testiranje i Metrika Softvera",ExamImagePath="/images/professor_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{DepartmentID = 1, ExamName="Dizajn Softvera",ExamImagePath="/images/arrival_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{ExamID = 7, DepartmentID = 1, ExamName="Teorija Algoritama Automata i Jezika",ExamImagePath="/images/chat_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1 },
        //        new Exam{ExamID = 7, DepartmentID = 2, ExamName="Teorija Algoritama Automata i Jezika",ExamImagePath="/images/chat_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1 },
        //        new Exam{DepartmentID = 2, ExamName="Kriptografski Algoritmi",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{DepartmentID = 1, ExamName="Metgodika Programiranja",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{DepartmentID = 2, ExamName="Metgodika Programiranja",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{DepartmentID = 1, ExamName="Matematicka Logika",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{DepartmentID = 2, ExamName="Matematicka Logika",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{DepartmentID = 1, ExamName="Metodika Elektronskog Ucena",ExamImagePath="/images/student_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{DepartmentID = 2, ExamName="Metodika Elektronskog Ucena",ExamImagePath="/images/student_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{DepartmentID = 1, ExamName="Racunarska Grafika 1",ExamImagePath="/images/student_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{DepartmentID = 2, ExamName="Racunarska Grafika 1",ExamImagePath="/images/student_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{DepartmentID = 1, ExamName="Baze Podataka",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{DepartmentID = 2, ExamName="Baze Podataka",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //    };
        //    Exams.ForEach(e => context.Exams.Add(e));
        //    context.SaveChanges();
        //}

        //protected override void Seed(ExamDbContext context)
        //{

        //    var Users = new List<User>
        //    {
        //        new User{FullName="Slobodan Stankovic",Email="slobodan.stankovic@pmf.edu.rs",IndexNo="71RN",IsProfessor=false,Password="slobodan123",Title="Student of MSc"},
        //        new User{FullName="Djordje Marinkovic",Email="djordje.marinkovic@pmf.edu.rs",IndexNo="63RN",IsProfessor=false,Password="slobodan123",Title="Student of MSc"},
        //        new User{FullName="Nikola Djordjevic",Email="nikola.djordjevic@pmf.edu.rs",IndexNo="73RN",IsProfessor=false,Password="slobodan123",Title="Student of MSc"},
        //        new User{FullName="Marko Petkovic",Email="marko.petkovic@pmf.edu.rs",IsProfessor=true,Password="slobodan123",Title="Full Professor"},
        //        new User{FullName="Marko Milosevic",Email="marko.milosevic@pmf.edu.rs",IsProfessor=true,Password="slobodan123",Title="Full Professor"},
        //        new User{FullName="Svetozar Rancic",Email="svetozar.rancic@pmf.edu.rs",IsProfessor=true,Password="slobodan123",Title="Full Professor"},
        //        new User{FullName="Jelena Ignjatovic",Email="jelena.ignjatovic@pmf.edu.rs",IsProfessor=true,Password="slobodan123",Title="Full Professor"}

        //    };
        //    Users.ForEach(u => context.Users.Add(u));
        //    var Exams = new List<Exam>
        //    {
        //        new Exam{Department="Razvoj Softvera",ExamName="Teorija Programskih Jezika",ExamImagePath="/images/professor_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{Department="Razvoj Softvera",ExamName="Razvoj Veb Aplikacija",ExamImagePath="/images/arrival_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{Department="Razvoj Softvera",ExamName="Razvoj Mobilnih Aplikacija",ExamImagePath="/images/chat_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=2 },
        //        new Exam{Department="Upravljanje Informacijama",ExamName="Teorija Informacija i Kodiranje",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{Department="Razvoj Softvera",ExamName="Testiranje i Metrika Softvera",ExamImagePath="/images/professor_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{Department="Razvoj Softvera",ExamName="Dizajn Softvera",ExamImagePath="/images/arrival_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{Department="Razvoj Softvera",ExamName="Teorija Algoritama Automata i Jezika",ExamImagePath="/images/chat_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1 },
        //        new Exam{Department="Upravljanje Informacijama",ExamName="Kriptografski Algoritmi",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},


        //    };
        //    Exams.ForEach(e => context.Exams.Add(e));
        //    context.SaveChanges();
        //}
    }
}
