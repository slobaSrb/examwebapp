namespace ExamWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Chat",
                c => new
                    {
                        ChatID = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        ExamID = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                        DateShown = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ChatID)
                .ForeignKey("dbo.Exam", t => t.ExamID, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserID, cascadeDelete: true)
                .Index(t => t.ExamID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Exam",
                c => new
                    {
                        ExamID = c.Int(nullable: false, identity: true),
                        ExamName = c.String(),
                        YearOfStudies = c.Int(nullable: false),
                        Semester = c.Int(nullable: false),
                        LevelOfStudies = c.String(),
                        ExamImagePath = c.String(),
                    })
                .PrimaryKey(t => t.ExamID);
            
            CreateTable(
                "dbo.CheckIn",
                c => new
                    {
                        CheckInID = c.Int(nullable: false, identity: true),
                        CheckInDateTime = c.DateTime(nullable: false),
                        CheckOutDateTime = c.DateTime(),
                        ExamID = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                        FilePath = c.String(),
                    })
                .PrimaryKey(t => t.CheckInID)
                .ForeignKey("dbo.Exam", t => t.ExamID, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserID, cascadeDelete: true)
                .Index(t => t.ExamID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        IndexNo = c.String(),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        LastName = c.String(nullable: false, maxLength: 50),
                        Email = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        ProtocolNo = c.String(),
                    })
                .PrimaryKey(t => t.UserID);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        RoleID = c.Int(nullable: false, identity: true),
                        RoleName = c.String(),
                    })
                .PrimaryKey(t => t.RoleID);
            
            CreateTable(
                "dbo.Department",
                c => new
                    {
                        DepartmentID = c.Int(nullable: false, identity: true),
                        DepartmentName = c.String(),
                    })
                .PrimaryKey(t => t.DepartmentID);
            
            CreateTable(
                "dbo.UserExam",
                c => new
                    {
                        UserRefId = c.Int(nullable: false),
                        ExamRefId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserRefId, t.ExamRefId })
                .ForeignKey("dbo.User", t => t.UserRefId, cascadeDelete: true)
                .ForeignKey("dbo.Exam", t => t.ExamRefId, cascadeDelete: true)
                .Index(t => t.UserRefId)
                .Index(t => t.ExamRefId);
            
            CreateTable(
                "dbo.RoleUser",
                c => new
                    {
                        RoleRefId = c.Int(nullable: false),
                        UserRefId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RoleRefId, t.UserRefId })
                .ForeignKey("dbo.Role", t => t.RoleRefId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserRefId, cascadeDelete: true)
                .Index(t => t.RoleRefId)
                .Index(t => t.UserRefId);
            
            CreateTable(
                "dbo.DepartmentExam",
                c => new
                    {
                        DepartmentRefId = c.Int(nullable: false),
                        ExamRefId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DepartmentRefId, t.ExamRefId })
                .ForeignKey("dbo.Department", t => t.DepartmentRefId, cascadeDelete: true)
                .ForeignKey("dbo.Exam", t => t.ExamRefId, cascadeDelete: true)
                .Index(t => t.DepartmentRefId)
                .Index(t => t.ExamRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Chat", "UserID", "dbo.User");
            DropForeignKey("dbo.Chat", "ExamID", "dbo.Exam");
            DropForeignKey("dbo.DepartmentExam", "ExamRefId", "dbo.Exam");
            DropForeignKey("dbo.DepartmentExam", "DepartmentRefId", "dbo.Department");
            DropForeignKey("dbo.CheckIn", "UserID", "dbo.User");
            DropForeignKey("dbo.RoleUser", "UserRefId", "dbo.User");
            DropForeignKey("dbo.RoleUser", "RoleRefId", "dbo.Role");
            DropForeignKey("dbo.UserExam", "ExamRefId", "dbo.Exam");
            DropForeignKey("dbo.UserExam", "UserRefId", "dbo.User");
            DropForeignKey("dbo.CheckIn", "ExamID", "dbo.Exam");
            DropIndex("dbo.DepartmentExam", new[] { "ExamRefId" });
            DropIndex("dbo.DepartmentExam", new[] { "DepartmentRefId" });
            DropIndex("dbo.RoleUser", new[] { "UserRefId" });
            DropIndex("dbo.RoleUser", new[] { "RoleRefId" });
            DropIndex("dbo.UserExam", new[] { "ExamRefId" });
            DropIndex("dbo.UserExam", new[] { "UserRefId" });
            DropIndex("dbo.CheckIn", new[] { "UserID" });
            DropIndex("dbo.CheckIn", new[] { "ExamID" });
            DropIndex("dbo.Chat", new[] { "UserID" });
            DropIndex("dbo.Chat", new[] { "ExamID" });
            DropTable("dbo.DepartmentExam");
            DropTable("dbo.RoleUser");
            DropTable("dbo.UserExam");
            DropTable("dbo.Department");
            DropTable("dbo.Role");
            DropTable("dbo.User");
            DropTable("dbo.CheckIn");
            DropTable("dbo.Exam");
            DropTable("dbo.Chat");
        }
    }
}
