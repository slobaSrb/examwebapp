namespace ExamWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedTokensTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Token",
                c => new
                    {
                        TokenID = c.Int(nullable: false, identity: true),
                        TokenValue = c.String(),
                        EndTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.TokenID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Token");
        }
    }
}
