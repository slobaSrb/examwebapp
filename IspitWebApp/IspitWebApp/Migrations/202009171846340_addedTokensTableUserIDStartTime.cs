namespace ExamWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedTokensTableUserIDStartTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Token", "StartTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.Token", "UserID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Token", "UserID");
            DropColumn("dbo.Token", "StartTime");
        }
    }
}
