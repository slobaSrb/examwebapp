namespace ExamWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserDepartment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DepartmentUser",
                c => new
                    {
                        DepartmentRefId = c.Int(nullable: false),
                        UserRefId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DepartmentRefId, t.UserRefId })
                .ForeignKey("dbo.Department", t => t.DepartmentRefId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserRefId, cascadeDelete: true)
                .Index(t => t.DepartmentRefId)
                .Index(t => t.UserRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DepartmentUser", "UserRefId", "dbo.User");
            DropForeignKey("dbo.DepartmentUser", "DepartmentRefId", "dbo.Department");
            DropIndex("dbo.DepartmentUser", new[] { "UserRefId" });
            DropIndex("dbo.DepartmentUser", new[] { "DepartmentRefId" });
            DropTable("dbo.DepartmentUser");
        }
    }
}
